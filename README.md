# gbfs-influx

Fetches GBFS (General Bikeshare Feed Specification) station information and feeds it to an InfluxDB database.

## Build
env GOOS=linux GOARCH=amd64 go build -o bin/gbfs-influx

## Use

#### Create database
curl -i -XPOST http://localhost:8086/query --data-urlencode "q=CREATE DATABASE bysykkel"

#### Execute
 ./gbfs-influx influxUrl influxDatabase gpfsUrl gbfsClientName

## Example:
./gbfs-influx "http://localhost:8086" "bysykkel" "https://gbfs.urbansharing.com/oslobysykkel.no/station_status.json" "loremipsum"
