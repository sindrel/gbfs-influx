//
// Usage:
// ./gbfs-influx <influxUrl> <influxDatabase> <gpfsUrl> <gbfsClientName>
//
// Example:
// ./gbfs-influx "http://localhost:8086" "bysykkel" "https://gbfs.urbansharing.com/oslobysykkel.no/station_status.json" "loremipsum"
//

package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// StationStatus holds status values about a station
type StationStatus struct {
	StationID         string `json:"station_id"`
	IsInstalled       int    `json:"is_installed"`
	IsRenting         int    `json:"is_renting"`
	IsReturning       int    `json:"is_returning"`
	LastReported      int    `json:"last_reported"`
	NumBikesAvailable int    `json:"num_bikes_available"`
	NumDocksAvailable int    `json:"num_docks_available"`
}

// Station holds data about a station
type Station struct {
	Stations []StationStatus `json:"stations"`
}

// StationsStatusList holds a list of stations
type StationsStatusList struct {
	LastUpdated int     `json:"last_updated"`
	TimeToLive  int     `json:"ttl"`
	Data        Station `json:"data"`
}

// AddToDatabase posts station status data to an Influx database
func AddToDatabase(station StationStatus) {
	fmt.Println("-> Parsing Station ID: " + station.StationID)

	bikesAvailStr := strconv.Itoa(station.NumBikesAvailable)
	docksAvailStr := strconv.Itoa(station.NumDocksAvailable)

	now := time.Now()
	nanoStr := strconv.FormatInt(now.UnixNano(), 10)

	queryBikesAvail := "stations,station=" + station.StationID + " bikes_available=" + bikesAvailStr + " " + nanoStr
	PostToInfluxAPI(queryBikesAvail)

	queryDocksAvail := "stations,station=" + station.StationID + " docks_available=" + docksAvailStr + " " + nanoStr
	PostToInfluxAPI(queryDocksAvail)
}

// PostToInfluxAPI posts a query to a InfluxDB HTTP API
func PostToInfluxAPI(query string) {
	body := strings.NewReader(query)

	req, err := http.NewRequest("POST", os.Args[1]+"/write?db="+os.Args[2], body)
	if err != nil {
		panic(err)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	r, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()
}

func main() {
	fmt.Println("./gbfs-influx <influxUrl> <influxDatabase> <gpfsUrl> <gbfsClientName>")
	fmt.Println("Fetching data from API...")

	req, _ := http.NewRequest("GET", os.Args[3], nil)
	req.Header.Add("client-name", os.Args[4])

	r, _ := http.DefaultClient.Do(req)

	decoder := json.NewDecoder(r.Body)

	var statusList StationsStatusList
	err := decoder.Decode(&statusList)
	if err != nil {
		panic(err)
	}

	defer r.Body.Close()

	fmt.Println("Posting result to time series database...")
	for s := range statusList.Data.Stations {
		AddToDatabase(statusList.Data.Stations[s])
	}

	fmt.Println("Done")
}
